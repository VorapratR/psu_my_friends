import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./page/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'feeds',
    loadChildren: () => import('./page/main/feeds/feeds.module').then( m => m.FeedsPageModule)
  },
  {
    path: 'plan',
    loadChildren: () => import('./page/main/plan/plan.module').then( m => m.PlanPageModule)
  },
  {
    path: 'event',
    loadChildren: () => import('./page/main/event/event.module').then( m => m.EventPageModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./page/user/user.module').then( m => m.UserPageModule)
  },
  {
    path: 'the-plan',
    loadChildren: () => import('./page/the-plan/the-plan.module').then( m => m.ThePlanPageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
