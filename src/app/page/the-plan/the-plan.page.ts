import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { CrudService } from '../../crud.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-the-plan',
  templateUrl: './the-plan.page.html',
  styleUrls: ['./the-plan.page.scss'],
})
export class ThePlanPage implements OnInit {
  userName: any;
  planId: any;
  planTitle: any;
  planDescription: any;
  planType: any;
  planLocation: any;
  planStart: any;
  planEnd: any;
  planPeople: any;
  planImg: any;
  planUser: any;
  planUserImg: any;
  planDate: any;
  planJoin: any;
  planJoinName: any;

  strName: any;
  strNum: any;
  myDateS:any;
  myDateE:any;


  constructor(
    private storage: Storage,
    private crudService: CrudService,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
    this.storage.get('name').then((val) => {
      this.userName = val;
    });

    this.storage.get('viewPlan').then((val) => {
      console.log(val);
      this.crudService.read_plans().subscribe(data => {
        data.map(e => {
          if (e.payload.doc.id === val) {
            console.log(e.payload.doc.id);
            this.planId = e.payload.doc.id;
            this.planTitle = e.payload.doc.data()['title'];
            this.planDescription = e.payload.doc.data()['description'];
            this.planType = e.payload.doc.data()['type'];
            this.planLocation = e.payload.doc.data()['location'];
            this.myDateS = new Date(e.payload.doc.data()['start']);
            this.planStart = this.myDateS.getMonth()+ '/' +this.myDateS.getDate()+ '/' +this.myDateS.getFullYear()+ ' ' +this.formatAMPM(this.myDateS);
            this.myDateE = new Date(e.payload.doc.data()['end']);
            this.planEnd = this.myDateE.getMonth()+ '/' +this.myDateE.getDate()+ '/' +this.myDateE.getFullYear()+ ' ' +this.formatAMPM(this.myDateE);
            this.planPeople = e.payload.doc.data()['people'];
            this.planImg = e.payload.doc.data()['img'];
            this.planUser = e.payload.doc.data()['user'];
            this.planUserImg = e.payload.doc.data()['userImg'];
            this.planDate = e.payload.doc.data()['date'];
            this.planJoin = e.payload.doc.data()['join'];
            this.planJoinName = e.payload.doc.data()['joinName'];
          }
        });
      });
    });
  }
  resetData() {
    this.planId = '';
    this.planTitle = '';
    this.planDescription = '';
    this.planType = '';
    this.planLocation = '';
    this.planStart = '';
    this.planEnd = '';
    this.planPeople = '';
    this.planImg = '';
    this.planUser = '';
    this.planUserImg = '';
    this.planDate = '';
    this.planJoin = '';
    this.storage.remove('viewPlan');
  }
  joinPlan(id) {
    if(parseInt(this.planJoin) < parseInt(this.planPeople)){
      const numPeople = parseInt(this.planJoin) + 1;
      this.strNum = numPeople.toString();
      this.crudService.update_plan(id, { 'join' : this.strNum });
      this.strName = this.planJoinName + '$' + this.userName;
      this.crudService.update_plan(id, { 'joinName' : this.strName });
      this.presentAlertConfirm();
    } else {
      this.presentAlertConfirms();
    }
    
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Status',
      message: 'Join success',
    });

    await alert.present();
  }
  async presentAlertConfirms() {
    const alert = await this.alertController.create({
      header: 'Status',
      message: 'Join Fail',
    });

    await alert.present();
  }

  canclePlan(id) {
    this.crudService.delete_plan(id);
  }

  formatAMPM(date) { // This is to display 12 hour format like you asked
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }
}

