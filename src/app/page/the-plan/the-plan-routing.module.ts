import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThePlanPage } from './the-plan.page';

const routes: Routes = [
  {
    path: '',
    component: ThePlanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThePlanPageRoutingModule {}
