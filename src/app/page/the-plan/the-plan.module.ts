import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ThePlanPageRoutingModule } from './the-plan-routing.module';

import { ThePlanPage } from './the-plan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ThePlanPageRoutingModule
  ],
  declarations: [ThePlanPage]
})
export class ThePlanPageModule {}
