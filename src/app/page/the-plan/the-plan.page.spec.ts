import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ThePlanPage } from './the-plan.page';

describe('ThePlanPage', () => {
  let component: ThePlanPage;
  let fixture: ComponentFixture<ThePlanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThePlanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ThePlanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
