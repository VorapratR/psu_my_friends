import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  name: any;
  pic: any;
  constructor(
    private storage: Storage,
    private fireAuth: AngularFireAuth,
    private router: Router,
    ) { }

  ngOnInit() {
    this.storage.get(`name`).then((val) => {
      this.name = val;
    });
    this.storage.get(`pic`).then((val) => {
      this.pic = val;
    });
  }

  logoutWithFacebook() {
    this.fireAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
      this.storage.remove('name');
      this.storage.remove('pic');
    });
  }

}
