
import { Component } from '@angular/core';
import { Camera } from '@ionic-native/Camera/ngx';
import { ActionSheetController} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CrudService } from '../../../crud.service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.page.html',
  styleUrls: ['./plan.page.scss'],
})
export class PlanPage  {

  base64Image = '';

  structure: string;
  text = 0;
  public planing: FormGroup;

  userName = '';
  userPic = '';

  constructor(
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    private formBuilder: FormBuilder,
    private storage: Storage,
    private crudService: CrudService
  ) {
    this.planing = this.formBuilder.group({
      title: [''],
      description: [''],
      type: [''],
      location: [''],
      start: [''],
      end: [''],
      people: [''],
      img: [''],
      user: [''],
      userImg: [''],
      date: [''],
      joinName : [''],
    });
    this.storage.get(`name`).then((val) => {
      this.userName = val;
    });
    this.storage.get(`pic`).then((val) => {
      this.userPic = val;
    });
  }

  AccessCamera() {
    this.camera.getPicture({
      targetWidth: 800,
      targetHeight: 800,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL
    }).then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
        console.log(err);
      });
  }

   AccessGallery() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL
    }).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.planing.value.img = this.base64Image;
      }, (err) => {
      console.log(err);
      });
   }

  formatDate(date: string) {
    const data: string = date;
    const monthNames = [
      'Jan', 'Feb', 'Mar',
      'Apr', 'May', 'Jun', 'Jul',
      'Aug', 'Sep', 'Oct',
      'No', 'Dec'
    ];
    const day = data.slice(3, 5);
    // tslint:disable-next-line:radix
    const monthIndex =  parseInt(data.slice(0, 2))-1 ;
    return monthNames[monthIndex] + ' ' + day;
  }

   CreateRecord() {
    this.planing.value.date = this.formatDate(this.planing.value.start.slice(5, 10));
    this.planing.value.user = this.userName;
    this.planing.value.userImg = this.userPic;
    //this.planing.value.img = this.base64Image;
    this.planing.value.join = "0";
    const record = this.planing.value;
    console.log(record);
    this.crudService.create_Newplan(record).then(resp => {
      this.planing.reset();
      this.planing.value.title = '' ;
      this.planing.value.description = '' ;
      this.planing.value.type = '' ;
      this.planing.value.location = '' ;
      this.planing.value.start = '' ;
      this.planing.value.end = '' ;
      this.planing.value.people = '' ;
      this.planing.value.img = '' ;
      this.planing.value.join = '' ;
      this.planing.value.joinName = '' ;
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }

  RemoveRecord(rowID) {
    this.crudService.delete_plan(rowID);
  }
  /*
  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.Name;
    record.EditAge = record.Age;
    record.EditAddress = record.Address;
  }
  UpdateRecord(recordRow) {
    let record = {};
    record['Name'] = recordRow.EditName;
    record['Age'] = recordRow.EditAge;
    record['Address'] = recordRow.EditAddress;
    this.crudService.update_plan(recordRow.id, record);
    recordRow.isEdit = false;
  }
  */

}
