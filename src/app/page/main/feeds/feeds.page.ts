import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { CrudService } from '../../../crud.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.page.html',
  styleUrls: ['./feeds.page.scss'],
})
export class FeedsPage implements OnInit {
  user: any = {};
  userName: any;
  userPic: any;
  plans = [];
  filterData = [];
  search = '';

  constructor(
    private fireAuth: AngularFireAuth,
    private router: Router,
    private storage: Storage,
    public alertController: AlertController,
    private crudService: CrudService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.fireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.user = {
          id: user.providerId,
          uid: user.uid,
          photoURL: user.photoURL,
          email: user.email,
          displayName: user.displayName,
          creationTime: user.metadata.creationTime,
          lastSignInTime: user.metadata.lastSignInTime,
          refreshToken: user.refreshToken,
          phoneNumber: user.phoneNumber,
        };
      } else {
          this.router.navigate(['/login']);
      }

      this.storage.set(`name`, this.user.displayName);
      this.storage.get(`pic`).then((val) => {
        this.userPic = val;
      });
    });

    this.crudService.read_plans().subscribe(data => {

      this.plans = data.map(e => {
        return {
          id: e.payload.doc.id,
          title: e.payload.doc.data()['title'],
          description: e.payload.doc.data()['description'],
          type: e.payload.doc.data()['type'],
          location: e.payload.doc.data()['location'],
          start: e.payload.doc.data()['start'],
          end: e.payload.doc.data()['end'],
          people: e.payload.doc.data()['people'],
          img: e.payload.doc.data()['img'],
          user: e.payload.doc.data()['user'],
          userImg: e.payload.doc.data()['userImg'],
          date: e.payload.doc.data()['date'],
          join: e.payload.doc.data()['join'],
          joinName: e.payload.doc.data()['joinName'],
        };
      });
      this.filterData = this.plans;
    });
  }

  logoutWithFacebook() {
    this.fireAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
      this.storage.remove('name');
      this.storage.remove('pic');
    });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Logout',
      message: 'You want logout this app?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.logoutWithFacebook();
          }
        }
      ]
    });

    await alert.present();
  }

  inputSearch(event) {
    const result = event.target.value;
    this.search = result;
    this.setFilteredLocations();
  }

  setFilteredLocations(){
    this.filterData = this.plans.filter((item) => {
      return item.title.toLowerCase().indexOf(this.search.toLowerCase()) > -1;
    });
    console.log(this.filterData);
    console.log(this.search);
  }

  async viewPlans(id) {
    console.log("id : " + id);
    await this.storage.set('viewPlan', id);
    await this.navCtrl.navigateForward(['/the-plan']);
  }
}
