import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { CrudService } from '../../../crud.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss'],
})
export class EventPage implements OnInit {
  userPic: any;
  plans: any;
  userName: any;
  joinUser = [];
  constructor(
    private fireAuth: AngularFireAuth,
    private router: Router,
    private storage: Storage,
    public alertController: AlertController,
    private crudService: CrudService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.storage.get(`pic`).then((val) => {
      this.userPic = val;
    });
    this.storage.get('name').then((val) => {
      this.userName = val;
    });

    this.crudService.read_plans().subscribe(data => {

      this.plans = data.map(e => {
        return {
          id: e.payload.doc.id,
          title: e.payload.doc.data()['title'],
          description: e.payload.doc.data()['description'],
          type: e.payload.doc.data()['type'],
          location: e.payload.doc.data()['location'],
          start: e.payload.doc.data()['start'],
          end: e.payload.doc.data()['end'],
          people: e.payload.doc.data()['people'],
          img: e.payload.doc.data()['img'],
          user: e.payload.doc.data()['user'],
          userImg: e.payload.doc.data()['userImg'],
          date: e.payload.doc.data()['date'],
          join: e.payload.doc.data()['join'],
          joinName: e.payload.doc.data()['joinName'],
        };
      });
      console.log(this.plans);
      this.plans.forEach(element => {
        const data = element.joinName;
        this.joinUser.push(data.split("$"));
        console.log("eiei"+this.joinUser);
      });
    });
  }

  logoutWithFacebook() {
    this.fireAuth.auth.signOut().then(() => {
      this.router.navigate(["/login"]);
      this.storage.remove('name');
      this.storage.remove('pic');
    })
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Logout',
      message: 'You want logout this app?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.logoutWithFacebook();
          }
        }
      ]
    });

    await alert.present();
  }

  async viewPlan(id) {
    await this.storage.set('viewPlan', id);
    await this.navCtrl.navigateForward(['/the-plan']);
  }

}
