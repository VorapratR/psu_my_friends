import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loading: any;
  constructor(
    private fb: Facebook,
    private router: Router,
    public loadingController: LoadingController,
    private fireAuth: AngularFireAuth,
    private storage: Storage
  ) {}

  async ngOnInit() {
    this.loading = this.loadingController.create({
      message: 'Connecting ...'
    });
  }

  async presentLoading(loading) {
    await loading.present();
  }

  async loginWithFacebook() {
    this.fb.login(['public_profile', 'email'])
      .then((response: FacebookLoginResponse) => {
        if (response.status = 'connected') {
          this.storage.set(`pic`, 'https://graph.facebook.com/' + response.authResponse.userID + '/picture?type=large');
        }
        this.onLoginSuccess(response);
        console.log(response.authResponse.accessToken);
      }).catch((error) => {
        console.log(error);
      });
  }

  onLoginSuccess(res: FacebookLoginResponse) {
      // const { token, secret } = res;
      const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
      this.fireAuth.auth.signInWithCredential(credential)
        .then((response) => {
          this.router.navigate(['/app/tabs/feeds']);
          this.loading.dismiss();
        });
  }
}
