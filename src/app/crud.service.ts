import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class CrudService {

    constructor(
        private firestore: AngularFirestore
    ) { }


    create_Newplan(record) {
        return this.firestore.collection('plan').add(record);
    }

    read_plans() {
        return this.firestore.collection('plan').snapshotChanges();
    }

    update_plan(recordID, record) {
        this.firestore.doc('plan/' + recordID).update(record);
    }

    delete_plan(record_id) {
        this.firestore.doc('plan/' + record_id).delete();
    }
}